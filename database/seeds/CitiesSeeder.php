<?php

use Illuminate\Database\Seeder;
use App\Cities;

class CitiesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $json = File::get('database/data/cities.json');
        $data = json_decode($json);
        
        foreach($data->cities as $obj){
            Cities::updateOrCreate([
                'id' => $obj->id,
                'name' => $obj->name,
                'state_id' => $obj->state_id,
            
            ]);
        }
    }
}
