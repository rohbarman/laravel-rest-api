<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

use App\Countries;
class CountriesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {   
        $json = File::get('database/data/countries.json');
        $data = json_decode($json);
        
        foreach($data->countries as $obj){
            
            Countries::updateOrCreate([
                'id' => $obj->id,
                'sortname' => $obj->sortname,
                'name' => $obj->name,
                'phoneCode' => $obj->phoneCode,
            ]);
        }
    }   
}
