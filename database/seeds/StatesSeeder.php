<?php

use Illuminate\Database\Seeder;
use App\States;
class StatesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $json = File::get('database/data/states.json');
        $data = json_decode($json);
        
        foreach($data->states as $obj){
            
            States::updateOrCreate([
                'id' => $obj->id,
                'name' => $obj->name,
                'country_id' => $obj->country_id,
            ]);
        }
    }
}
