<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\States;
use App\Cities;
use Illuminate\Support\Facades\DB;

class InfoController extends Controller
{
    public function states(){
        $states = States::where('country_id', 101)->get()->toArray();
        return response()->json([
            'states' => $states
        ]);
       
    }

    public function cities($state_id){

        $state =  DB::table('cities')
                    ->select('cities.name')
                    ->join('states', 'cities.state_id', '=', 'states.id')
                    ->join('countries', 'states.country_id', '=', 'countries.id')
                    ->where('states.id', '=', $state_id)
                    ->where('countries.id', '=', 101)
                    ->toSql();
        dd($state);
        if(!$state){
            return response()->json([
                'message' => __('State id is invalid')
            ], 404);
        }
        
        $states = Cities::where('state_id', $state_id)->get()->toArray();
        return response()->json([
            'states' => $states
        ]);
    }
}
